import cv2 as cv
import numpy as np

img = cv.imread('img2.jpg')


def show_image(image, name="Image"):
    cv.imshow(name, image)
    cv.waitKey(0)
    cv.destroyAllWindows()


show_image(img)

            # Show Blue Channel
blue = img.copy()
blue[:, :, 1] = 0
blue[:, :, 2] = 0

show_image(blue, "Blue")


            # Show Grayscale

gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
# show_image(gray, "Grayscale")

blur = cv.GaussianBlur(gray, (9, 9), 0)
show_image(blur)


            # Show Rotated

(h, w) = img.shape[:2]

print(h, " ", w)
rotation = cv.getRotationMatrix2D((w / 2, h / 2), 90, 1.0)
rotated = cv.warpAffine(img, rotation, (h, w))
show_image(rotated, "Rotated")

            # Show resized

resize = cv.resize(img, (2*h, w), interpolation=cv.INTER_AREA)
show_image(resize, "Resize")

            # Show edges

edges = cv.Canny(img, 100, 200)

show_image(edges, "Edge")






ret, thresh = cv.threshold(gray, 0, 255, cv.THRESH_BINARY_INV + cv.THRESH_OTSU)

kernel = np.ones((3, 3), np.uint8)
opening = cv.morphologyEx(thresh, cv.MORPH_OPEN, kernel, iterations=2)

sure_bg = cv.dilate(opening, kernel, iterations=3)

dist_transform = cv.distanceTransform(opening, cv.DIST_L2, 5)
ret, sure_fg = cv.threshold(dist_transform, 0.7 * dist_transform.max(), 255, 0)

sure_fg = np.uint8(sure_fg)
unknown = cv.subtract(sure_bg, sure_fg)

# Marker labelling
ret, markers = cv.connectedComponents(sure_fg)
# Add one to all labels so that sure background is not 0, but 1
markers = markers + 1
# Now, mark the region of unknown with zero
markers[unknown == 255] = 0

new_img = img.copy()

markers = cv.watershed(new_img, markers)
new_img[markers == -1] = [255, 0, 0]
show_image(new_img, "Segment")








face_cascade = cv.CascadeClassifier('files/haarcascade_frontalface_default.xml')
eye_cascade = cv.CascadeClassifier('files/haarcascade_eye.xml')

faces = face_cascade.detectMultiScale(gray, 1.3, 5)
for (x, y, w, h) in faces:
    cv.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), 2)
    roi_gray = gray[y:y + h, x:x + w]
    roi_color = img[y:y + h, x:x + w]
    eyes = eye_cascade.detectMultiScale(roi_gray)
    for (ex, ey, ew, eh) in eyes:
        cv.rectangle(roi_color, (ex, ey), (ex + ew, ey + eh), (0, 255, 0), 2)
show_image(img, "Face")

vidcap = cv.VideoCapture("mar.mp4")

for i in range(20):
    success, image = vidcap.read()
    print(success)
    cv.imshow('capture', image)
    cv.waitKey(500)

cv.destroyAllWindows()